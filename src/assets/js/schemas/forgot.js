const forgotSchema = {
    fields: [{
            type: "input",
            inputType: "email",
            label: "Email",
            model: "email",
            id: "email",
            placeholder: "Your email address",
            required: true,
            validator: ["email"],
            styleClasses: ["col-12"],            
        }
    ]
}


export default forgotSchema;