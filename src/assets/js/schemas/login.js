const loginSchema = {
    fields: [{
            type: "input",
            inputType: "text",
            label: "",
            model: "username",
            id: "username",
            placeholder: "Email address",
            required: true,
            validator: ["required"],
            attributes: {
                "autocomplete":"current-username",
            },
            styleClasses: ["col-12"],
        },
        {
            type: "input",
            inputType: "password",
            label: "",
            model: "password",
            id: "password",
            placeholder: "Password",
            required: true,
            validator: ["string"],
            attributes: {
                "autocomplete":"current-password",
            },
            styleClasses: ["col-12"],
        },
    ]
}


export default loginSchema;