const resetSchema = {
    fields: [{
            type: "input",
            inputType: "password",
            label: "Password",
            model: "passwod",
            id: "password",
            placeholder: "**********",
            required: true,
            validator: ["string"],
            styleClasses: ["col-12"],            
        }
    ]
}


export default resetSchema;