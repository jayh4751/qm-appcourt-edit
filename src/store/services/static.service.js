import axios from 'axios';
const API_URL = process.env.VUE_APP_API_URL;
const HEADERS = {
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
}
class StaticService {

    /**
     *  getContent (Static content) from table 'content' with the identifier param[identifier] additional params for the query WHERE OREDRBY, ETC can be added to the param[query]
     * @param {string} identifier 
     * @param {object} query_string 
     */
    getFeature(identifier) {
        let object = {
            "guid": localStorage.getItem('liSession'),
            "entity": "appcourt/api_feature",
            "where": `identifier='${identifier}' & deleted_at=#null`
         }
        return axios
            .post(`${API_URL}?-a=query`,object, HEADERS)
            .then(function (response) {
                response.data.data[0].properties.body = JSON.parse(response.data.data[0].properties.body);
                return Promise.resolve(response.data.data[0].properties);
            }).catch(error => {
                return Promise.reject(error);
            });
    }

}
export default new StaticService();