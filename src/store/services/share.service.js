import axios from "axios";

const API_URL = process.env.VUE_APP_API_URL;
const HEADERS = {
  headers: {
    "Content-Type": "application/x-www-form-urlencoded",
  },
};

class ShareService {
  getShare(object) {
    const payload = {
      identifier: object,
    };
    return axios
      .post(`${API_URL}?-a=getShare`, payload, HEADERS)
      .then((response) => {
        return Promise.resolve(response.data);
      });
  }
  getPresentation(object) {
    return axios
      .post(API_URL + "?-a=loadByPK", object, HEADERS)
      .then((response) => {
        return Promise.resolve(response.data);
      });
  }
  loadByPk(object) {
    return axios
      .post(API_URL + "?-a=loadByPK", object, HEADERS)
      .then((response) => {
        return Promise.resolve(response.data);
      });
  }
  query(object) {
    return axios
      .post(API_URL + "?-a=query", object, HEADERS)
      .then((response) => {
        return Promise.resolve(response.data);
      });
  }
  getPresentationWithInvite(object) {
    const payload = object;

    return axios
      .post(API_URL + "presentation/scene/getWithInvite", payload, HEADERS)
      .then((response) => {
        return Promise.resolve(response.data);
      });
  }
  getNotes(object) {
    return axios
      .post(API_URL + "?-a=query", object, HEADERS)
      .then((response) => {
        return Promise.resolve(response.data);
      });
  }
  createNote(object) {
    return axios
        .post(API_URL + '?-a=insert', object, HEADERS)
        .then(function (response) {
            return Promise.resolve(response.data);
        }).catch(error => {
            return Promise.reject(error);
        });
  }
}
export default new ShareService();
