import Vue from 'vue';
import Vuex from 'vuex';

import {
    auth
} from './modules/auth.module';
import {
    liStatic
} from './modules/liStatic.module';
import {
    liDynamic
} from './modules/liDynamic.module';
import {
    liPresentation
} from './modules/liPresentation.module';

import {
    liDatabase
} from './modules/liDatabase.module';

import {
    liShare
} from './modules/liShare.module';



Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        auth: auth,
        liStatic: liStatic,
        liDynamic: liDynamic,
        liDatabase: liDatabase,
        liPresentation:liPresentation,
        liShare:liShare,
    }
});