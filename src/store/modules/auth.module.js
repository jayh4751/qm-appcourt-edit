import AuthService from "../services/auth.service";
export const auth = {
  namespaced: true,
  state: {
    status: {
      loggedIn: localStorage.getItem("liSession") ? true : false,
      timeCounter: 0,
      needsLogin: null,
    },
    account: null,
    user: null,
  },
  actions: {
    login({ commit }, user) {
      return AuthService.login(user).then(
        (user) => {
          commit("loginSuccess", user);
          return Promise.resolve(user);
        },
        (error) => {
          commit("loginFailure");
          return Promise.reject(error);
        }
      );
    },
    reconnect({ commit }) {
      if (localStorage.getItem("liSession") != null) {
        return AuthService.reconnect().then(
          (user) => {
            if ("error" in user) {
              commit("loginFailure");
              return Promise.reject(user.error);
            } else {
              // status
              commit("loginSuccess", user);
              return Promise.resolve(user);
            }
          },
          (error) => {
            commit("loginFailure");
            return Promise.reject(error);
          }
        );
      } else {
          commit("loginFailure");
        return Promise.reject('no session');
      }
    },
    logout({ commit }) {
      AuthService.logout();
      commit("logout");
    },
    SET_ACOUNT({ commit }, account) {
      commit("setAccount", account);
      return Promise.resolve();
    },
    forgotPassword({ commit }, email) {
      return AuthService.forgotPassword(email).then(
        (response) => {
          commit("emptyFn");
          return Promise.resolve(response);
        },
        (error) => {
          commit("loginFailure");
          return Promise.reject(error);
        }
      );
    },
  },
  mutations: {
    loginSuccess(state, user) {
      state.status.needsLogin = false;
      state.status.loggedIn = true;
      state.user = user;
      if (localStorage.getItem("liSessionAccount") != null) {
        state.account = localStorage.getItem("liSessionAccount");
      } else {
        state.account = user.accounts[0].id;
        localStorage.setItem(
          "liSessionAccount",
          user.accounts[0].id
        );
      }
    },
    loginFailure(state) {
      state.status.loggedIn = false;
      state.status.needsLogin = true;
      state.user = null;
      state.account = 0;
    },
    logout(state) {
      state.status.loggedIn = false;
      state.user = null;
      state.account = 0;
      localStorage.removeItem("liSessionAccount");
      localStorage.removeItem("liSession");
    },
    loggedInStatus(state, status) {
      state.status.needsLogin = status;
    },
    setAccount(state, obj) {
      state.account = obj;
      localStorage.setItem("liSessionAccount", state.account);
    },
    emptyFn() {
      console.log("empty");
    },
  },
  getters: {
    getAccount: (state) => {
      return state.account;
    },
    getUser: (state) => {
      if (state.user != null) {
        return state.user;
      } else {
        return "error";
      }
    },
    isLoggedIn: (state) => {
      return state.status.loggedIn;
    },
    loggedInTimer: (state) => {
      return state.status.timeCounter;
    },
    loggedInStatus: (state) => {
      return state.status.needsLogin;
    },
  },
};
