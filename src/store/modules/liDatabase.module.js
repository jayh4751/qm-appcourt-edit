import DatabaseService from '../services/database.service';
export const liDatabase = {
  namespaced: true,
  state: {
    databases: {}
  },
  actions: {
    GET_ALL({
        commit,
      },
      _data) {
      const _vm = (_data.vm) ? _data.vm : '';
      return DatabaseService
        .getAll()
        .then(response => {
            commit('setDatabases', response);
            return Promise.resolve(response.data);
          },
          error => {
            _vm.$bvToast.toast(error.statusText, {
              title: "Error",
              variant: "danger",
              autoHideDelay: 300,
              toaster: 'b-toaster-bottom-right'
            });
          });
    },
    UPDATE_FIELD({
      commit,
    },
    _data) {
    const _vm = (_data.vm) ? _data.vm : '';
    const _object = (_data.object) ? _data.object : '';
    const _field = (_data.field) ? _data.field : '';
    return DatabaseService
      .updateField({field:_field, object:_object})
      .then(response => {
          commit('emptyFn');
          return Promise.resolve(response.data);
        },
        error => {
          _vm.$bvToast.toast(error.statusText, {
            title: "Error",
            variant: "danger",
            autoHideDelay: 300,
            toaster: 'b-toaster-bottom-right'
          });
        });
  },

  },
  mutations: {
    emptyFn: () => {
      return true;
  },
    setDatabases(state, data) {
      state.databases = data.payload;
    },
  },
  getters: {
    getDatabases: (state) => {
      return state.databases;
    },
  }
};