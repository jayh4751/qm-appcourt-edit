# flowchart-diagram documentation

## table of content

1. properties
2. Making new nodes
3. making new link-markers

---

## 2 Making new nodes

1. Go to src/JayhsWork/node.vue => 31 and add a svg with these properies:  
   ( v-if="node.shape === '.....'" //give the node a name  
    :x="x"  
    :y="y"  
    :style="svgNodeStyle(node)"  
    :height="" // depends on the node  
    :width="" //depends on the node  
   )
2. Add a path depending if a path needs to be set with the property: d
3. add an if statement with the right returns (look at the other if statements for inspiration) in the functions:

- textPosition() -> 325 (for the text inside of the node)
- invisibleRecShapeByNode() -> 272 ( for the deletebutton and the new link button)

4. Go to src/components/liBuilderDiagram.vue and change these functions:

- nodeAnchorPointsWithHeightAndWidth(node) -> 978 ( positioning for: top/bottom/left/right of the node)
- arowDownAndTrashCanPosition(nodeX, nodeY, node) -> 1181 (positioning for the new link button and the delete button on the node)
- newLineFromNodeType(fromNodeId) -> 1219 (this will return the cordinates from where the newline will spawn from)

5. Go to src/JayhsWork/NodeSettings.vue -> 200 ( and add a value and text to shapes)

---

## Making new link-markers
