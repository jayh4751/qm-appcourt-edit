export default {
  methods: {
    /**
     * Opens a modal
     *
     * @param {options} Object
     *                  Available options are::
     *                  headerTitle : String => the title of the modal | mandatory
     *                  headerBgVariant : String => the body background variant of the modal. Can be any of the following "primary", "secondary", "danger", "success", "warning","info" | mandatory
     *                  headerTextVariant : String => the header text variant of the modal. Can be any of the following "primary", "secondary", "danger", "success", "warning","info" | mandatory
     *                  bodyBgVariant : String => the body background variant of the modal. Can be any of the following "primary", "secondary", "danger", "success", "warning","info" | mandatory
     *                  size: : String => the size variant of the modal. Can be any of the following "sm", "md", "lg", "xl" | mandatory
     *                  scrollable: : Boolean | mandatory
     *                  noCloseOnEsc: Boolean | mandatory
     *                  noCloseOnBackdrop: Boolean | mandatory
     *                  hideHeaderClose: Boolean | mandatory
     *                  hideBackdrop: Boolean | mandatory
     * options :{
     *    headerTitle : "",
     *    headerBgVariant: "white",
     *    headerTextVariant:"primary",
     *    bodyBgVariant: "white",
     *    size: "md",
     *    scrollable: false,
     *    noCloseOnEsc: false,
     *    noCloseOnBackdrop: false,
     *    hideHeaderClose: false,
     *    hideBackdrop: false
     * }
     * 
     */

    openModal(options) {
      return new Promise((resolve) => {
        this.$store.state.liStatic.liModal.headerTitle = options.headerTitle;
        this.$store.state.liStatic.liModal.headerBgVariant =
          options.headerBgVariant;
        this.$store.state.liStatic.liModal.headerTextVariant =
          options.headerTextVariant;
        this.$store.state.liStatic.liModal.bodyBgVariant = options.bodyBgVariant;
        this.$store.state.liStatic.liModal.size = options.size;
        this.$store.state.liStatic.liModal.scrollable = options.scrollable;
        this.$store.state.liStatic.liModal.noCloseOnEsc = options.noCloseOnEsc;
        this.$store.state.liStatic.liModal.noCloseOnBackdrop = options.noCloseOnBackdrop;
        this.$store.state.liStatic.liModal.hideHeaderClose = options.hideHeaderClose;
        this.$store.state.liStatic.liModal.hideBackdrop = options.hideBackdrop
        this.$bvModal.show('liModal');
        resolve();
      });
    },
    /**
     * Closes a modal
     *
     * @param {reset} String : => whether to reset to state of the modal | mandatory
     *
     */
    closeModal(options) {
      if (options.reset) {
        return new Promise((resolve) => {
            this.resetModal().then(() => {
              this.$bvModal.hide('liModal');
              resolve();
            });
        });
      } else {
        return new Promise((resolve) => {
          this.$nextTick(() => {
            this.$bvModal.hide('liModal');
            resolve();
          });
        });
      }
    },
    /**
     * Resets a modal to it's initial state
     */
    resetModal() {
      return new Promise((resolve) => {
        this.$store.state.liStatic.liModal.headerTitle = "";
        this.$store.state.liStatic.liModal.headerBgVariant = "";
        this.$store.state.liStatic.liModal.headerTextVariant = "";
        this.$store.state.liStatic.liModal.bodyBgVariant = "";
        this.$store.state.liStatic.liModal.size = "";
        this.$store.state.liStatic.liModal.scrollable = true;
        this.$store.state.liStatic.liModal.noCloseOnEsc = false;
        this.$store.state.liStatic.liModal.noCloseOnBackdrop = false;
        this.$store.state.liStatic.liModal.hideHeaderClose = false;
        this.$store.state.liStatic.liModal.hideBackdrop = true;
        resolve();
      });
    },
  },
}