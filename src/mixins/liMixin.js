export default {
    data() {
        return {
            ACTIVE_ACCOUNT:0,
            DEFAULT_COLORS:[
                '#ffffff','#e9ecef ','#ced4da ','#adb5bd ','#6c757d ','#495057 ','#343a40 ','#000000', // grays
                '#3B5A9E','#6610f2 ','#e83e8c ','#dc3545 ',' #fd7e14 ','#ffc107 ','#17a2b8','#28a745 ' // colors
            ],
            APP_TITLE: process.env.VUE_APP_TITLE,
            MONTH_NAMES: ['January', 'February', 'March', 'April', 'May', 'June','July', 'August', 'September', 'October', 'November', 'December'],
            LINK_TARGETS: [
                {
                    label: "Same window",
                    type: "_self",
                },
                {
                    label: "New window",
                    type: "_blank",
                },
            ],
            WIDGET_SETTINGS: {
                styleClasses: "",
                settings: {
                    borderSizeLinked: true,
                    borderRadiusLinked: true,
                    marginLinked: true,
                    paddingLinked: true,
                },
                classes: {
                    shadow: false,
                    horizontalAlign: false,
                    verticalAlign: false,
                },
                inlineStyles: {
                    backgroundColor: "",
                    textAlign: "",
                    borderWidth: "",
                    borderTopWidth: "",
                    borderRightWidth: "",
                    borderBottomWidth: "",
                    borderLeftWidth: "",
                    borderRadius: "",
                    borderTopLeftRadius: "",
                    borderTopRightRadius: "",
                    borderBottomRightRadius: "",
                    borderBottomLeftRadius: "",
                    borderStyle: null,
                    borderColor: "",
                    top: "",
                    right: "",
                    bottom: "",
                    left: "",
                    margin: "",
                    marginTop: "",
                    marginRight: "",
                    marginBottom: "",
                    marginLeft: "",
                    padding: "",
                    paddingTop: "",
                    paddingRight: "",
                    paddingBottom: "",
                    paddingLeft: "",
                },
            },
            WIDGET_MODAL_OPTIONS: {
                headerTitle: "Settings",
                headerBgVariant: "white",
                headerTextVariant: "dark",
                size: "lg",
                scrollable: false,
                noCloseOnEsc: true,
                noCloseOnBackdrop: true,
                hideHeaderClose: true,
                hideBackdrop:false,
            },
            WIDGET_MODAL_ITEM_OPTIONS: {
                headerTitle: "Item settings",
                headerBgVariant: "white",
                headerTextVariant: "dark",
                size: "lg",
                scrollable: false,
                noCloseOnEsc: true,
                noCloseOnBackdrop: true,
                hideHeaderClose: true,
                hideBackdrop:false,
            },
            CONTENT_SLIDE_SETTINGS: {
                styleClasses: "",
                settings: {
                    borderSizeLinked: true,
                    borderRadiusLinked: true,
                    marginLinked: true,
                    paddingLinked: true,
                },
                classes: {
                    shadow: false,
                    horizontalAlign: false,
                    verticalAlign: false,
                },
                inlineStyles: {
                    backgroundColor: "#FFFFFF",
                    textAlign: "",
                    borderWidth: "",
                    borderTopWidth: "",
                    borderRightWidth: "",
                    borderBottomWidth: "",
                    borderLeftWidth: "",
                    borderRadius: "",
                    borderTopLeftRadius: "",
                    borderTopRightRadius: "",
                    borderBottomRightRadius: "",
                    borderBottomLeftRadius: "",
                    borderStyle: null,
                    borderColor: "",
                    top: "",
                    right: "",
                    bottom: "",
                    left: "",
                    margin: "",
                    marginTop: "",
                    marginRight: "",
                    marginBottom: "",
                    marginLeft: "",
                    padding: "",
                    paddingTop: "",
                    paddingRight: "",
                    paddingBottom: "",
                    paddingLeft: "",
                },
            }
        }
    },
    computed: {
        app_config() {
            return this.$store.getters["liStatic/getAppConfig"];
        },
        loggedInTimer(){
            return this.$store.getters["auth/loggedInTimer"];
        },
        loggedInStatus:{
            get(){
                return this.$store.getters["auth/loggedInStatus"];
            },
            set(status){
                this.$store.commit("auth/loggedInStatus", status);
            }
            
        },
        app_user() {
            return this.$store.getters["auth/getUser"];
        },
        liLoading: {
            get() {
                return this.$store.getters["liStatic/getLoading"];
            },
            set(status) {
                this.$store.commit("liStatic/setLoading", status);
            },
        },
        liPresenting: {
            get() {
                return this.$store.getters["liStatic/getPresenting"];
            },
            set(status) {
                this.$store.commit("liStatic/setPresenting", status);
            },
        },
        builderActive: {
            get() {
                return this.$store.getters["liStatic/getBuilderStatus"];
            },
            set(status) {
                this.$store.commit("liStatic/setBuilderStatus", status);
            }
        },

    },
    methods: {
        removeKeyStartsWith(obj, prefix) {
            Object.keys(obj).forEach(function(key) {
              //if(key[0]==prefix) delete obj[key];////without regex
              if (key.match("^" + prefix)) delete obj[key]; //with regex
            });
          },
        /**
         * [!] USE WITH CAUTION [!] 
         * Returns a deep find of a key from a dot notation string 
         * @param {obj} Object the object where to look in for the key
         * @param {is} String the  "key.key.key" notation for the key to look for in the given object
         * 
         */
        pathIndex(obj,is) {   // obj,'1.2.3' -> multiIndex(obj,['1','2','3'])
            return this.multiIndex(obj,is.split('.'))
        },
        multiIndex(obj,is) {  // obj,['1','2','3'] -> ((obj['1'])['2'])['3']
            return is.length ? this.multiIndex(obj[is[0]],is.slice(1)) : obj
        },
        /**
         * Returns a widgetId
         * 
         */
        generateWidgetId() {
            return "liGW_" + this.generateUUID();
        },
        /**
         * Returns a random string with [Aa-Zz][0-9] characters
         * 
         * @param {length} Number a number for the length of the string to be created
         *
         */
        getRandomString(length) {
            var randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var result = '';
            for (var i = 0; i < length; i++) {
                result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
            }
            return result;
        },
        /**
         * Returns a GUID 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'
         */
        generateUUID: function () { // Public Domain/MIT
            var d = new Date().getTime();
            if (typeof performance !== 'undefined' && typeof performance.now === 'function') {
                d += performance.now(); //use high-precision timer if available
            }
            var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = (d + Math.random() * 16) % 16 | 0;
                d = Math.floor(d / 16);
                return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
            });
            return uuid;
        },
        /**
         * Returns a true if value contains only numbers
         * 
         * @param {value} ANY the item to be tested for numbers
         *
         */
        isNumeric(value) {
            return /^-?\d+$/.test(value);
        },
        /**
         * Returns a true if the key exists in the object
         * 
         * @param {obj} Object the object to be searched
         * @param {key} String the string to be searched for in the given object
         *
         */
        containsKey(obj, key) {
            return Object.keys(obj).includes(key);
        },
        /**
         * Returns a formatted title string
         * 
         * @param {prefix} String a prefix for the title
         * @param {suffix} String a suffix for the title
         *
         */
        createPageTitle(prefix = '', suffix = '') {
            const _APP_TITLE = process.env.VUE_APP_TITLE;
            const _PREFIX = (prefix != '') ? prefix + ' | ' : '';
            const _SUFFIX = (suffix != '') ? ' | ' + suffix : '';
            const TITLE = _PREFIX + _APP_TITLE + _SUFFIX;

            return TITLE;
        },
        /**
         * Returns a String with the first character capitalized
         * 
         * @param {string} String The string that needs to be formatted
         *
         */
        capitalize(string) {
            if (typeof string !== 'string') return ''
            // return string.charAt(0).toUpperCase() + string.slice(1)
            return this._.capitalize(string);
        },
        /**
         * Returns a formatted date string
         * 
         * @param {date} String The date that needs to be formatted
         * @param {prefomattedDate} String A prefix for the return // default false == ''
         * @param {hideYear} boolean To hide or show the year in the return // default false == show the year
         */
        getFormattedDate(date, prefomattedDate = false, hideYear = false) {
            const day = date.getDate();
            const month = this.MONTH_NAMES[date.getMonth()];
            const year = date.getFullYear();
            const hours = date.getHours();
            let minutes = date.getMinutes();

            if (minutes < 10) {
                // Adding leading zero to minutes
                minutes = `0${ minutes }`;
            }

            if (prefomattedDate) {
                // Today at 10:20
                // Yesterday at 10:20
                return `${ prefomattedDate } at ${ hours }:${ minutes }`;
            }

            if (hideYear) {
                // 10. January at 10:20
                return `${ day }. ${ month } at ${ hours }:${ minutes }`;
            }

            // 10. January 2017. at 10:20
            return `${ day }. ${ month } ${ year }. at ${ hours }:${ minutes }`;
        },
        /**
         * Returns a "time ago" date string
         * 
         * @param {dateParam} String The date that needs to be formatted
         *
         */
        timeAgo(dateParam) {
            if (!dateParam) {
                return null;
            }

            const date = typeof dateParam === 'object' ? dateParam : new Date(dateParam);
            const DAY_IN_MS = 86400000; // 24 * 60 * 60 * 1000
            const today = new Date();
            const yesterday = new Date(today - DAY_IN_MS);
            const seconds = Math.round((today - date) / 1000);
            const minutes = Math.round(seconds / 60);
            const isToday = today.toDateString() === date.toDateString();
            const isYesterday = yesterday.toDateString() === date.toDateString();
            const isThisYear = today.getFullYear() === date.getFullYear();

            if (seconds < 5) {
                return 'now';
            } else if (seconds < 60) {
                return `${ seconds } seconds ago`;
            } else if (seconds < 90) {
                return 'about a minute ago';
            } else if (minutes < 60) {
                return `${ minutes } minutes ago`;
            } else if (isToday) {
                return this.getFormattedDate(date, 'Today'); // Today at 10:20
            } else if (isYesterday) {
                return this.getFormattedDate(date, 'Yesterday'); // Yesterday at 10:20
            } else if (isThisYear) {
                return this.getFormattedDate(date, false, true); // 10. January at 10:20
            }

            return this.getFormattedDate(date); // 10. January 2017. at 10:20
        },
        /**
         * Returns a truncaed string string
         * 
         * @param {str} String The string that needs to be trencated
         * @param {num} int The number of charackters
         */
        truncateString(str, num) {
            if (str.length <= num) {
                return str
            }
            return str.slice(0, num) + '...'
        },
        /**
         * Returns the text from a HTML string
         * 
         * @param {html} String The html string
         */
        stripHtml(html) {
            // Create a new div element
            var temporalDivElement = document.createElement("div");
            // Set the HTML content with the providen
            temporalDivElement.innerHTML = html;
            // Retrieve the text property of the element (cross-browser support)
            return temporalDivElement.textContent || temporalDivElement.innerText || "";
        },
        getContrastYIQ(hexcolor) {
            const color = hexcolor.replace("#", "");
            var r = parseInt(color.substr(0, 2), 16);
            var g = parseInt(color.substr(2, 2), 16);
            var b = parseInt(color.substr(4, 2), 16);
            var yiq = (r * 299 + g * 587 + b * 114) / 1000;
            return yiq >= 128 ? "black" : "white";
        },
        /**
         * Add a value (string) to the users local clipboard
         * 
         * @param {str} String  The string that needs to be copied
         *
         * !IMPORTANT NOTE
         * This function doesn't work within an active modal
         * 
         */
        copyToClipboard(str) {
            const el = document.createElement('textarea');
            el.value = str;
            el.setAttribute('readonly', '');
            el.style.position = 'absolute';
            el.style.left = '-9999px';
            document.body.appendChild(el);
            el.select();
            document.execCommand('copy');
            document.body.removeChild(el);
            this.$bvToast.toast(str + " Is copied to clipboard.", {
                title: "Copied to clipboard",
                variant: "",
                solid: true,
                toaster: "b-toaster-bottom-right",
                appendToast: true
            });
        }
    }
}