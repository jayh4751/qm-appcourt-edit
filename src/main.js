import "@babel/polyfill";
import "mutationobserver-shim";
import Vue from "vue";
import "./plugins/bootstrap-vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

// BOOTSTRAP ICONS
import { IconsPlugin } from "bootstrap-vue";
Vue.use(IconsPlugin);

// THIRD PARTY STUFF
// FONTAWESOME LOCAL
import "./assets/vendor/fontawesome-free-5.15.1-web/css/all.min.css";

// VUE LODASH
import VueLodash from "vue-lodash";
import lodash from "lodash";
Vue.use(VueLodash, { lodash: lodash });

// VUE Draggable / Sortable.js
import draggable from "vuedraggable";
Vue.component("draggable", draggable);

import CKEditor from "@ckeditor/ckeditor5-vue2";
Vue.use(CKEditor);

// VUE Portal
import PortalVue from "portal-vue";
Vue.use(PortalVue);

// VUE HEADFULL
import vueHeadful from "vue-headful";
Vue.component("vue-headful", vueHeadful);

// VUE-FORM-GENERATOR
import VueFormGenerator from "vue-form-generator";
Vue.use(VueFormGenerator);

// VUE-FORM-GENERATOR CUSTOM FIELDS
import textAreaWithBuilder from "./components/forms/vfg-fields/textAreaWithBuilder.vue";
Vue.component("field-textAreaWithBuilder", textAreaWithBuilder);

import vSelect from "vue-select";
// Set the components prop default to return our fresh components
vSelect.props.components.default = () => ({
  Deselect: {
    render: (createElement) => createElement("b-icon-x"),
  },
  OpenIndicator: {
    render: (createElement) => createElement("b-icon-chevron-down"),
  },
});
Vue.component("v-select", vSelect);
// import 'vue-select/dist/vue-select.css';

// VUE FRIENDLY IFRAME
import VueFriendlyIframe from "vue-friendly-iframe";
Vue.use(VueFriendlyIframe);

// GRIDSTACK
import "gridstack/dist/gridstack.css";
import liGridStack from "./components/gridstack/liGridStack.vue";
Vue.component("liGridStack", liGridStack);

// DROPZONE
import vue2Dropzone from "vue2-dropzone";
Vue.component("vueDropzone", vue2Dropzone);
import "vue2-dropzone/dist/vue2Dropzone.min.css";

// VUE COLOR
import { Compact } from "vue-color";
Vue.component("compactColorPicker", Compact);
import { Sketch } from "vue-color";
Vue.component("sketchColorPicker", Sketch);

// LINNOX STUFF
// LINNOX MIXINS
import liMixin from "./mixins/liMixin";
Vue.mixin(liMixin);

import liModalMixin from "./mixins/liModalMixin";
Vue.mixin(liModalMixin);

import VueWindowSize from "vue-window-size";
Vue.use(VueWindowSize);

import resize from "vue-element-resize-detector";
Vue.use(resize);

// STATIC COMPONENTS
import liLoader from "./components/structure/loader";
Vue.component("liLoader", liLoader);

import liComponentLoader from "./components/structure/componentLoader.vue";
Vue.component("liComponentLoader", liComponentLoader);

import liNavbar from "./components/structure/navbar";
Vue.component("liNavbar", liNavbar);

import liModal from "./components/structure/modal";
Vue.component("liModal", liModal);

import liLoginModal from "./components/structure/liLoginModal";
Vue.component("liLoginModal", liLoginModal);

// SHARE STATIC COMPONENTS
import liNavbarShare from "./components/structure/navbarShare";
Vue.component("liNavbarShare", liNavbarShare);

// Builder
// Builder base widget
import liWidgetHeader from "./components/builderWidgets/base/header/liWidgetHeader.vue";
Vue.component("liWidgetHeader", liWidgetHeader);
// Widget base settings tab
import liWidgetTabSettings from "./components/builderWidgets/base/modal/tabs/liWidgetTabSettings.vue";
Vue.component("liWidgetTabSettings", liWidgetTabSettings);
// Widget base footer
import liWidgetFooter from "./components/builderWidgets/base/modal/footer/liWidgetFooter.vue";
Vue.component("liWidgetFooter", liWidgetFooter);

// Builder custom widgets
// Form
import liBuilderForm from "./components/builderWidgets/liBuilderForm.vue";
Vue.component("liBuilderForm", liBuilderForm);
// Table
import liBuilderTable from "./components/builderWidgets/liBuilderTable.vue";
Vue.component("liBuilderTable", liBuilderTable);
// Text
import liBuilderText from "./components/builderWidgets/liBuilderText.vue";
Vue.component("liBuilderText", liBuilderText);
// File == Image
import liBuilderFile from "./components/builderWidgets/liBuilderFile.vue";
Vue.component("liBuilderFile", liBuilderFile);
// Document == File download
import liBuilderDocument from "./components/builderWidgets/liBuilderDocument.vue";
Vue.component("liBuilderDocument", liBuilderDocument);
// Buttons
import liBuilderButtons from "./components/builderWidgets/liBuilderButtons.vue";
Vue.component("liBuilderButtons", liBuilderButtons);
// Buttons settings Tab
import liBuilderButtonsTab from "./components/builderWidgets/liBuilderButtonsTab.vue";
Vue.component("liBuilderButtonsTab", liBuilderButtonsTab);
// Hotspots
import liBuilderHotspots from "./components/builderWidgets/liBuilderHotspots.vue";
Vue.component("liBuilderHotspots", liBuilderHotspots);
// Diagram
import liBuilderDiagram from "./components/builderWidgets/liBuilderDiagram.vue";
Vue.component("liBuilderDiagram", liBuilderDiagram);
// TODO fix
// Diagram settings Tab
import liBuilderDiagramTab from "./components/builderWidgets/liBuilderDiagramTab.vue";
Vue.component("liBuilderDiagramTab", liBuilderDiagramTab);
// iFrame
import liBuilderiFrame from "./components/builderWidgets/liBuilderiFrame.vue";
Vue.component("liBuilderiFrame", liBuilderiFrame);
// video
import liBuilderVideo from "./components/builderWidgets/liBuilderVideo.vue";
Vue.component("liBuilderVideo", liBuilderVideo);
// Object
import liBuilderObject from "./components/builderWidgets/liBuilderObject.vue";
Vue.component("liBuilderObject", liBuilderObject);

// Object dynamic
import liBuilderObjectDynamic from "./components/builderWidgets/liBuilderObjectDynamic.vue";
Vue.component("liBuilderObjectDynamic", liBuilderObjectDynamic);

// Builder live widgets
// FORM
import liBuilderFormLive from "./components/builderWidgets/live/liBuilderFormLive.vue";
Vue.component("liBuilderFormLive", liBuilderFormLive);
// TEXTAREA WITH BUILDER
import textAreaWithBuilderLive from "./components/forms/textAreaWithBuilder.vue";
Vue.component("textAreaWithBuilderLive", textAreaWithBuilderLive);
// TABLE
import liBuilderTableLive from "./components/builderWidgets/live/liBuilderTableLive.vue";
Vue.component("liBuilderTableLive", liBuilderTableLive);
// OBJECT
import liBuilderObjectLive from "./components/builderWidgets/live/liBuilderObjectLive.vue";
Vue.component("liBuilderObjectLive", liBuilderObjectLive);
// TEXT
import liBuilderTextLive from "./components/builderWidgets/live/liBuilderTextLive.vue";
Vue.component("liBuilderTextLive", liBuilderTextLive);
// IMAGE
import liBuilderFileLive from "./components/builderWidgets/live/liBuilderImageLive.vue";
Vue.component("liBuilderFileLive", liBuilderFileLive);
// DOCUMENT
import liBuilderDocumentLive from "./components/builderWidgets/live/liBuilderDocumentLive.vue";
Vue.component("liBuilderDocumentLive", liBuilderDocumentLive);
// HOTSPOTS
import liBuilderHotspotsLive from "./components/builderWidgets/live/liBuilderHotspotsLive.vue";
Vue.component("liBuilderHotspotsLive", liBuilderHotspotsLive);
// BUTTONS
import liBuilderButtonsLive from "./components/builderWidgets/live/liBuilderButtonsLive.vue";
Vue.component("liBuilderButtonsLive", liBuilderButtonsLive);
// IFRAME
import liBuilderiFrameLive from "./components/builderWidgets/live/liBuilderiFrameLive.vue";
Vue.component("liBuilderIframeLive", liBuilderiFrameLive);
// VIDEO
import liBuilderVideoLive from "./components/builderWidgets/live/liBuilderVideoLive.vue";
Vue.component("liBuilderVideoLive", liBuilderVideoLive);
// DIAGRAM
import liBuilderDiagramLive from "./components/builderWidgets/live/liBuilderDiagramLive.vue";
Vue.component("liBuilderDiagramLive", liBuilderDiagramLive);

// PROCESS = SLIDES
import liProcessCanvas from "./components/process/liProcessCanvas.vue";
Vue.component("liProcessCanvas", liProcessCanvas);
import liProcess from "./components/process/liProcess.vue";
Vue.component("liProcess", liProcess);
import liProcessLink from "./components/process/liProcessLink.vue";
Vue.component("liProcessLink", liProcessLink);
import liProcessNode from "./components/process/liProcessNode.vue";
Vue.component("liProcessNode", liProcessNode);

// PRESENTATION
import liProcessCanvasLive from "./components/process/presentation/liProcessCanvasLive.vue";
Vue.component("liProcessCanvasLive", liProcessCanvasLive);
import liProcessLive from "./components/process/presentation/liProcessLive.vue";
Vue.component("liProcessLive", liProcessLive);
import liProcessLinkLive from "./components/process/presentation/liProcessLinkLive.vue";
Vue.component("liProcessLinkLive", liProcessLinkLive);
import liProcessNodeLive from "./components/process/presentation/liProcessNodeLive.vue";
Vue.component("liProcessNodeLive", liProcessNodeLive);

// BUTONS
import listItemEditButton from "./components/buttons/listItemEditButton";
Vue.component("listItemEditButton", listItemEditButton);
import listItemCloneButton from "./components/buttons/listItemCloneButton";
Vue.component("listItemCloneButton", listItemCloneButton);
import listItemMoveButton from "./components/buttons/listItemMoveButton";
Vue.component("listItemMoveButton", listItemMoveButton);
import listItemShareButton from "./components/buttons/listItemShareButton";
Vue.component("listItemShareButton", listItemShareButton);
import listItemDeleteButton from "./components/buttons/listItemDeleteButton";
Vue.component("listItemDeleteButton", listItemDeleteButton);

import listItemViewButton from "./components/buttons/listItemViewButton";
Vue.component("listItemViewButton", listItemViewButton);

// DROPZONE COMPONENTS
import liDropzoneBuilderImage from "./components/upload/liDropzoneBuilderImage.vue";
Vue.component("liDropzoneBuilderImage", liDropzoneBuilderImage);
import liDropzoneBuilderFile from "./components/upload/liDropzoneBuilderFile.vue";
Vue.component("liDropzoneBuilderFile", liDropzoneBuilderFile);

// INPUT FIELDS
import liColorFieldSmall from "./components/forms/liColorFieldSmall.vue";
Vue.component("liColorFieldSmall", liColorFieldSmall);

import liIconFieldSmall from "./components/forms/liIconFieldSmall.vue";
Vue.component("liIconFieldSmall", liIconFieldSmall);

// COMMENTS
import liComments from "./components/comments/liComments.vue";
Vue.component("liComments", liComments);

// DYNAMIC COMPONENTS
// LIST
import liList from "./components/list/list";
Vue.component("list", liList);
import liListItem from "./components/list/listItem";
Vue.component("listItem", liListItem);

// GRID
import liGrid from "./components/grid/grid";
Vue.component("liGrid", liGrid);
import liGridItem from "./components/grid/gridItem";
Vue.component("liGridItem", liGridItem);

// DASHBOARD
import liGridDashboard from "./components/dashboard/gridDashboard";
Vue.component("liGridDashboard", liGridDashboard);

// DATABASE
import lidatabaselist from "./components/database/lidatabaselist";
Vue.component("lidatabaselist", lidatabaselist);
import liTableItem from "./components/database/tableItem.vue";
Vue.component("liTableItem", liTableItem);
import liEditTable from "./components/database/liedittable.vue";
Vue.component("liEditTable", liEditTable);

// jayh's work eventbus
export const bus = new Vue();

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
